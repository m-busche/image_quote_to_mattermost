# Imagequote
#
# - Downloads a random image from Unsplash
# - Gets a (random) quote from a webservice
# - Draws the quote into th image
# - Posts the image into a Mattermost channel
"""
Author: Markus Busche @elpatron_kiel
License: MIT https://opensource.org/licenses/MIT, see LICENSE
"""

import os
import sys
import textwrap
import logging
import time
import random
from shutil import copyfile
from configparser import ConfigParser
from operator import itemgetter
import wikiquote
import requests
from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFilter
from unsplash_python.unsplash import Unsplash
from mattermostdriver import Driver


# Constants
LOG = None
CONFIGFILE = "imagequote.ini"


def get_random_image():
    """
    Downloads random image from unsplash
    Returns image filename, author and URL
    https://github.com/michael-hacker/unsplash-python
    """
    LOG.info("Getting random image from Unsplash")
    LOG.debug("Reading configuration")
    config = ConfigParser()
    config.read(CONFIGFILE)
    settings = dict(config.items("unsplash"))

    unsplash = Unsplash(
        {
            "application_id": settings["application_id"],
            "secret": settings["secret"],
            "callback_url": "urn:ietf:wg:oauth:2.0:oob",
        }
    )

    image = unsplash.photos().get_random_photo(width=500, query=settings["query"])

    if not image is None:
        imageurl = image[0]["urls"]["custom"]
    else:
        LOG.critical("Failed receiving image from Unsplash!")
        raise Exception("Failed receiving image from Unsplash!")

    # Modify custom link (set image width)
    imageurl = imageurl.replace("&w=500", "&w=" + settings["imagewidth"])
    imageauthor = image[0]["user"]["username"]
    LOG.debug("Image URL: %s", imageurl)
    LOG.debug("Image author: %s", imageauthor)

    imgreq = requests.get(imageurl)
    if imgreq.status_code == 200:
        with open("image.jpg", "wb") as imagefile:
            for chunk in imgreq:
                imagefile.write(chunk)
        LOG.debug("Successfully downloaded image file")
        return {"filename": imagefile.name, "author": imageauthor, "url": imageurl}
    else:
        LOG.error("Request error: %d", imgreq.status_code)
        raise Exception("Failed receiving image from Unsplash!")


def get_random_quote():
    """
    Get a random quote from https://taeglicheszit.at/zitat-api.php?format=json
    Returns author and quote
    Alternative: https://quotesondesign.com/api-v4-0/
    """
    LOG.info("Getting quote")
    LOG.debug("Reading configuration")
    config = ConfigParser()
    config.read(CONFIGFILE)
    settings = dict(config.items("quote"))
    quote_api = settings["api"].lower()
    api_settings = dict(config.items(quote_api))
    LOG.info("Using API: %s", quote_api)
    result = None

    if quote_api == "api_icndb":
        qjson = requests.get(api_settings["apiurl"]).json()
        if qjson != None:
            LOG.debug("Successfully retrieved quote")
            quote = qjson["value"]["joke"]
            quote = quote.replace("&quot:", '"')
            result = {"author": "http://www.icndb.com/", "quote": quote}
        else:
            LOG.error("Error getting quote")
    elif quote_api == "api_wikiquote_author":
        query = api_settings["filter"]
        author = api_settings["author"]
        lang = api_settings["lang"]
        quote = random.choice(wikiquote.quotes(query, lang=lang))
        result = {"author": author, "quote": quote}
    elif quote_api == "api_wikiquote_qod":
        lang = api_settings["lang"]
        qod = wikiquote.quote_of_the_day(lang=lang)
        result = {"author": qod[1], "quote": qod[0]}
    elif quote_api == "api_theysaidso":
        qjson = requests.get(api_settings["apiurl"]).json()
        if qjson != None:
            LOG.debug("Successfully retrieved quote")
            quote = qjson["contents"]["quotes"][0]["quote"]
            author = qjson["contents"]["quotes"][0]["author"]
            result = {"author": author, "quote": quote}
        else:
            LOG.error("Error getting quote")
    elif quote_api == "api_taeglicheszitat":
        qjson = requests.get(api_settings["apiurl"]).json()
        if qjson != None:
            LOG.debug("Successfully retrieved quote")
            result = {
                "author": qjson[api_settings["author"]],
                "quote": qjson[api_settings["quote"]],
            }
        else:
            LOG.error("Error getting quote")

    if not result is None:
        return result
    else:
        LOG.critical("Failed receiving quote!")
        raise Exception("Failed receiving quote!")


def add_quote_to_image(image, quote, author, attribution):
    """
    Inserts the quote into the image
    Returns: New image file name
    https://pedersenmark.com/batch-image-manipulation-with-python-famous-quotes-embedded-on-free-stock-images/
    https://community.unsplash.com/developersblog/how-to-give-proper-unsplash-attribution
    """
    LOG.info("Copying quote into image")
    LOG.debug("Reading configuration")

    config = ConfigParser()
    config.read(CONFIGFILE)
    settings = dict(config.items("quote"))

    img = Image.open(image)
    draw = ImageDraw.Draw(img)
    max_w, max_h = img.size[0], img.size[1]
    para = textwrap.wrap(quote, width=50)
    para.append("(" + author + ")")

    fontpath = os.path.normpath(settings["font"])
    # Does the font exist?
    if not os.path.isfile(fontpath):
        raise Exception("Font file not found (" + fontpath + "), check your settings!")

    fontsize = int(settings["fontsize"])
    shadowcolor = "black"
    font = ImageFont.truetype(fontpath, fontsize)

    current_h, pad = 50, 10
    LOG.info("Drawing text")
    for line in para:
        width, height = draw.textsize(line, font=font)
        # thin border (https://stackoverflow.com/questions/18974194/text-shadow-with-python)
        draw.text(
            ((max_w - width) / 2 - 1, current_h), line, font=font, fill=shadowcolor
        )
        draw.text(
            ((max_w - width) / 2 + 1, current_h), line, font=font, fill=shadowcolor
        )
        draw.text(
            ((max_w - width) / 2, current_h - 1), line, font=font, fill=shadowcolor
        )
        draw.text(
            ((max_w - width) / 2, current_h + 1), line, font=font, fill=shadowcolor
        )
        # draw text over border
        draw.text(((max_w - width) / 2, current_h), line, font=font)
        current_h += height + pad
    width, height = draw.textsize(attribution, font=font)
    draw.text(((max_w - width) / 2, max_h - 60), attribution, font=font)

    if not os.path.isdir(".\\images"):
        LOG.info("Creating image directory")
        os.makedirs(".\\images")

    newimagefile = os.path.normpath(
        ".\\images\\" + time.strftime("%Y%m%d-%H%M%S") + ".jpg"
    )
    img.save(newimagefile)
    LOG.debug("New image file saved as %s", newimagefile)

    # Add a drop shadow
    """
    dropshadow = str_to_bool(config.get('unsplash', 'dropshadow'))

    if dropshadow is True:
        LOG.info('Adding drop shadow to image')
        newimagefile = drop_shadow(newimagefile)
    """
    return newimagefile


def post_image_to_mattermost(image, imageurl):
    """
    Posts an image to a Mattermost channel
    https://github.com/Vaelor/python-mattermost-driver
    """
    LOG.info("Posting image to Mattermost")
    LOG.debug("Reading configuration")

    config = ConfigParser()
    config.read(CONFIGFILE)
    settings = dict(config.items("mattermost"))
    quote_api = config.get("quote", "api").lower()
    quotesource = config.get(quote_api, "name")
    quotesource_link = config.get(quote_api, "linked_url")
    headline = settings["headline"]
    imageurl += "?utm_source=imagequote&utm_medium=referral&utm_campaign=opensource"
    LOG.debug("Unsplash UTM URL: %s", imageurl)
    message = (
        "### " + headline + "\nBrought to you by [ImageQuote]"
        "(https://gitlab.com/m-busche/image_quote_to_mattermost), [Unsplash]("
        + imageurl
        + ") and ["
        + quotesource
        + "]("
        + quotesource_link
        + ")"
    )

    LOG.debug("Initializing Mattermost driver")
    mm_driver = Driver(
        {
            # Required options
            "url": settings["serveraddress"],
            "login_id": settings["login"],
            "password": settings["password"],
            # Optional / defaults to
            "scheme": settings["scheme"],
            "port": int(settings["port"]),
            "basepath": "/api/v4",
            # Use False if self signed/insecure certificate
            "verify": str_to_bool(settings["verify"]),
            # The interval the websocket will ping the server to keep the connection alive
            "timeout": 30,
        }
    )

    LOG.debug("Logging in")
    mm_driver.login()

    # To upload a file you will need to pass a `files` dictionary
    LOG.debug("Uploading image file")
    channel_name = settings["channel"]
    team_name = settings["team"]
    channel_id = mm_driver.api["channels"].get_channel_by_name_and_team_name(
        team_name, channel_name
    )["id"]
    LOG.debug("Getting file id")
    file_id = mm_driver.api["files"].upload_file(
        channel_id=channel_id, files={"files": (image, open(image, "rb"))}
    )["file_infos"][0]["id"]

    # track the file id and pass it in `create_post` options, to attach the file
    LOG.debug("Creating post")
    mm_driver.api["posts"].create_post(
        options={"channel_id": channel_id, "message": message, "file_ids": [file_id]}
    )

    return


# https://code.activestate.com/recipes/474116-drop-shadows-with-pil/
def drop_shadow(
    image_file,
    offset=(5, 5),
    background=0xffffff,
    shadow=0x444444,
    border=8,
    iterations=3,
):
    """
    Add a gaussian blur drop shadow to an image.

    image       - The image to overlay on top of the shadow.
    offset      - Offset of the shadow from the image as an (x,y) tuple.  Can be
                  positive or negative.
    background  - Background colour behind the image.
    shadow      - Shadow colour (darkness).
    border      - Width of the border around the image.  This must be wide
                  enough to account for the blurring of the shadow.
    iterations  - Number of times to apply the filter.  More iterations
                  produce a more blurred shadow, but increase processing time.
    """

    # Create the backdrop image -- a box in the background colour with a
    # shadow on it.
    image_file = os.path.normpath(image_file)
    img = Image.open(image_file)
    total_width = img.size[0] + abs(offset[0]) + 2 * border
    total_height = img.size[1] + abs(offset[1]) + 2 * border
    back = Image.new(img.mode, (total_width, total_height), background)

    # Place the shadow, taking into account the offset from the image
    shadow_left = border + max(offset[0], 0)
    shadow_top = border + max(offset[1], 0)
    back.paste(
        shadow,
        [shadow_left, shadow_top, shadow_left + img.size[0], shadow_top + img.size[1]],
    )

    # Apply the filter to blur the edges of the shadow.  Since a small kernel
    # is used, the filter must be applied repeatedly to get a decent blur.
    count = 0
    while count < iterations:
        back = back.filter(ImageFilter.BLUR)
        count += 1

    # Paste the input image onto the shadow backdrop
    image_left = border - min(offset[0], 0)
    image_top = border - min(offset[1], 0)
    back.paste(image_file, (image_left, image_top))
    back.save(image_file)

    return back


def str_to_bool(stringval):
    """
    Converts string to boolean
    """
    stringval = stringval.lower()
    if stringval == "true":
        return True
    elif stringval == "false":
        return False
    else:
        raise ValueError


def delete_old_files():
    """
    Delete old image files
    """
    config = ConfigParser()
    config.read(CONFIGFILE)
    settings = dict(config.items("cleanup"))

    if not str_to_bool(settings["cleanup"]):
        LOG.info("Cleanup disabled")
        return

    keep_files = int(settings["keepfiles"])
    LOG.info("Checking for old image files")

    dir_to_search = r".\images"
    filenames = []
    for path, subdirs, files in os.walk(dir_to_search):
        for name in files:
            filenames.append(os.path.join(path, name))
    # Sort the files according to the last modification time.
    sorted_files = sort_files_by_last_modified(filenames)

    delete_oldest_files(sorted_files, keep_files)


def sort_files_by_last_modified(file_list):
    """
    Given a list of files, return them sorted by the last
    modified times.
    """
    file_data = {}
    for fname in file_list:
        file_data[fname] = os.stat(fname).st_mtime

    file_data = sorted(file_data.items(), key=itemgetter(1))
    return file_data


def delete_oldest_files(sorted_file_list, keep=3):
    """
    Given a list of files sorted by last modified time and a number to
    keep, delete the oldest ones.
    """
    delete = len(sorted_file_list) - keep
    for count in range(0, delete):
        LOG.info("Deleting: %d files", sorted_file_list[count])
        os.remove(sorted_file_list[count][0])


def setup_custom_logger(name):
    """
    Set up custom logging
    """
    config = ConfigParser()
    config.read(CONFIGFILE)
    settings = dict(config.items("logging"))
    loglevel = logging.getLevelName(settings["level"])

    formatter = logging.Formatter(
        fmt="%(asctime)s %(levelname)-8s %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
    )
    handler = logging.FileHandler("imagequote.log", mode="w")
    handler.setFormatter(formatter)
    screen_handler = logging.StreamHandler(stream=sys.stdout)
    screen_handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(loglevel)
    logger.addHandler(handler)
    logger.addHandler(screen_handler)
    return logger


def check_ini_existance():
    """
    Checks if 'imagequote.ini' exists
    """
    filename = "imagequote.ini"
    if not os.path.isfile(filename):
        try:
            copyfile("imagequote.sample.ini", filename)
            print(
                "Default INI file ("
                + filename
                + ") created.\n\n"
                + 'You have to edit "'
                + filename
                + '" to get this up and running!'
            )
        except:
            print(
                filename
                + " not found,  copying "
                + "imagequote.sample.ini to"
                + filename
                + " failed."
            )
        return False
    else:
        return True


if __name__ == "__main__":
    if check_ini_existance() is False:
        sys.exit(1)

    LOG = setup_custom_logger("imagequote")
    LOG.info("Starting ImageQuote")
    delete_old_files()
    UNSPLASH_IMAGE = get_random_image()
    QUOTE = get_random_quote()
    IMAGE_OF_THE_DAY = add_quote_to_image(
        UNSPLASH_IMAGE["filename"],
        QUOTE["quote"],
        QUOTE["author"],
        "Photo by " + UNSPLASH_IMAGE["author"] + r" / Unsplash",
    )
    post_image_to_mattermost(IMAGE_OF_THE_DAY, UNSPLASH_IMAGE["url"])
    LOG.info("Done.")
